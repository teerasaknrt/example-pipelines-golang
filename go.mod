module example-pipelines-golang

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.7.1
)
