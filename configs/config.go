package configs

import (
	"fmt"
	"log"
	"strings"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

//Config models data for value from config in the environment
type Config struct {
	Debug    bool
	Env      string
	HTTPPort string
}

var config Config

//InitViper init the environment and config that used
func InitViper(path string, env string) {
	switch env {
	case "local":
		viper.SetConfigName("local-config")
		break
	case "develop":
		viper.SetConfigName("dev-config")
		break
	case "staging":
		viper.SetConfigName("staging-config")
		break
	default:
		viper.SetConfigName("config")
		break
	}
	log.Println("running on environment :", env)
	viper.AddConfigPath(path)
	viper.SetEnvPrefix("app")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	err = viper.Unmarshal(&config)
	if err != nil {
		fmt.Println(err)
	}
}

//GetViper get data from config
func GetViper() *Config {
	return &config
}
