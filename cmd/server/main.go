package main

import (
	"example-pipelines-golang/protocol"

	"github.com/sirupsen/logrus"
)

func main() {
	err := protocol.ServeHTTP()
	if err != nil {
		logrus.Fatal(err)
	}
}
