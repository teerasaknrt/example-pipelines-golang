package protocol

import (
	"context"
	"flag"
	"net/http"

	"example-pipelines-golang/configs"

	"github.com/gofiber/fiber/v2"
)

type config struct {
	Env string
}

func ServeHTTP() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	var cfg config
	flag.StringVar(&cfg.Env, "env", "", "the environment and config that used")
	flag.Parse()
	configs.InitViper("./configs", cfg.Env)

	app := fiber.New()

	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendStatus(http.StatusOK)
	})

	err := app.Listen(":" + configs.GetViper().HTTPPort)
	if err != nil {
		return err
	}
	return nil
}
